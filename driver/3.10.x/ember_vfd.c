/*
 *	Copyright (C) 2016-2019 Corey Moyer (cronmod.dev@gmail.com)
 *
 *	This file is part of Embedded Entertainment Rom Vacuum Fluorescent
 *	Display Driver (EmbER VFD Driver).
 *
 *	EmbER VFD Driver is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	EmbER VFD Driver is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with EmbER VFD Driver. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/types.h>
#include <linux/input.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/mutex.h>
#include <linux/errno.h>
#include <asm/irq.h>
#include <asm/io.h>
#include <linux/sched.h>	//wake_up_process()
#include <linux/kthread.h>	//kthread_create(),kthread_run()
#include <linux/err.h>		//IS_ERR(),PTR_ERR()
#include <mach/am_regs.h>
#include <mach/pinmux.h>
#include <linux/major.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/amlogic/aml_gpio_consumer.h>

#include "ember_vfd.h"

static int vfd_stb_pin_set_value(int value)
{
	if (value>0)
		SET_AOBUS_REG_MASK(AO_GPIO_O_EN_N, (1 << 26));
	else 
		CLEAR_AOBUS_REG_MASK(AO_GPIO_O_EN_N, (1 << 26));
	return 0;
}

static int vfd_clk_pin_set_value(int value)
{
	if (value>0)
		SET_AOBUS_REG_MASK(AO_GPIO_O_EN_N, (1 << 25));
	else 
		CLEAR_AOBUS_REG_MASK(AO_GPIO_O_EN_N, (1 << 25));
	return 0;
}

static int vfd_dat_pin_set_value(int value)
{
	if (value>0)
		SET_AOBUS_REG_MASK(AO_GPIO_O_EN_N, (1 << 27));
	else 
		CLEAR_AOBUS_REG_MASK(AO_GPIO_O_EN_N, (1 << 27));
	return 0;
}

static struct vfd_platform_data vfd_pdata =
{
	.set_stb_pin_value = vfd_stb_pin_set_value,
	.set_clk_pin_value = vfd_clk_pin_set_value,
	.set_dat_pin_value = vfd_dat_pin_set_value,
};

static const struct of_device_id vfd_match[] =
{
	{.compatible = "amlogic, ember_vfd"},
	{},
};

struct vfd
{
	char config_name[10];
	struct class *config_class;
	struct device *config_dev;
	int config_major;
	unsigned int debug_enable;
};

static struct vfd *gp_vfd=NULL;

// sysfs - clk
static char cur_clk_value[FP_LED_NUM];

static ssize_t vfd_clk_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	int size = sizeof(cur_clk_value)-1;
	return sprintf(buf, "%.*s\n", size, cur_clk_value);
}

static ssize_t vfd_clk_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int size = sizeof(cur_clk_value);
	char val[size];
	printk("EMBER_VFD: set clk <%s>\n", buf);
	sprintf(val, "I%s", buf);
	MDrv_FP_Update((char *)val);
	return sprintf(cur_clk_value, "%s", buf);
}

static DEVICE_ATTR(clk, S_IRUGO | S_IWUSR, vfd_clk_show, vfd_clk_store);

// sysfs - led
static char cur_led_value[FP_LED_NUM];

static ssize_t vfd_led_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	int size = sizeof(cur_led_value)-1;
	return sprintf(buf, "%.*s\n", size, cur_led_value);
}

static ssize_t vfd_led_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int size = sizeof(cur_led_value);
	char val[size];
	printk("EMBER_VFD: set led <%s>\n", buf);
	sprintf(val, "@%s", buf);
	MDrv_FP_Update((char *)val);
	return sprintf(cur_led_value, "%s", buf);
}

static DEVICE_ATTR(led, S_IRUGO | S_IWUSR, vfd_led_show, vfd_led_store);

static int vfd_config_open(struct inode *inode, struct file *file)
{
	file->private_data = gp_vfd;
	return 0;
}

static int vfd_config_release(struct inode *inode, struct file *file)
{
	file->private_data = NULL;
	return 0;
}

static const struct file_operations vfd_fops = {
	.owner      = THIS_MODULE,
	.open       = vfd_config_open,
	.release    = vfd_config_release,
};

static int register_vfd_dev(struct vfd *vfd)
{
	int ret = 0;
	strcpy(vfd->config_name,"ember_vfd");
	ret = register_chrdev(0,vfd->config_name,&vfd_fops);
	if (ret <= 0){
		printk("EMBER_VFD: failed to register device!\r\n");
		return ret;
	}
	vfd->config_major=ret;
	printk("EMBER_VFD: config major <%d>\r\n", ret);
	vfd->config_class=class_create(THIS_MODULE,vfd->config_name);
	vfd->config_dev=device_create(vfd->config_class,NULL,MKDEV(vfd->config_major,0),NULL,vfd->config_name);
	return ret;
}

static void ember_vfd_pinmux_init(void)
{
	//set mode GPIOAO
	aml_clr_reg32_mask(P_AO_RTI_PIN_MUX_REG,((1<<27)|(1<<28)|(1<<29)));
	//CLR GPIOAO_11
	CLEAR_AOBUS_REG_MASK(AO_GPIO_O_EN_N, (1 << 11));
	CLEAR_AOBUS_REG_MASK(AO_GPIO_O_EN_N, (1 << 27));
	//CLR GPIOAO_10
	CLEAR_AOBUS_REG_MASK(AO_GPIO_O_EN_N, (1 << 10));
	SET_AOBUS_REG_MASK(AO_GPIO_O_EN_N, (1 << 26));
	//CLR GPIOAO_9
	CLEAR_AOBUS_REG_MASK(AO_GPIO_O_EN_N, (1 << 9));
	SET_AOBUS_REG_MASK(AO_GPIO_O_EN_N, (1 << 25));
	//SET GPIOAO_11
	SET_AOBUS_REG_MASK(AO_RTI_PULL_UP_REG, (1 << 11));
	SET_AOBUS_REG_MASK(AO_RTI_PULL_UP_REG, (1 << 27));
}

static int vfd_probe(struct platform_device *pdev)
{
	struct vfd *vfd;
	int ret;
	struct vfd_platform_data *pdata = &vfd_pdata;
	if (!pdata){
		dev_err(&pdev->dev, "EMBER_VFD: platform data not found!\n");
		return -EINVAL;
	}
	if (!pdev->dev.of_node){
		dev_err(&pdev->dev, "EMBER_VFD: pdev->dev.of_node == NULL!\n");
		return -1;
	}
	if (hardware_init(pdata)){
		dev_err(&pdev->dev, "EMBER_VFD: hardware init failed!\n");
		return -EINVAL;
	}
	vfd = kzalloc(sizeof(struct vfd), GFP_KERNEL);
	if (!vfd) goto err1;
	gp_vfd = vfd;
	platform_set_drvdata(pdev, vfd);
	ret = device_create_file(&pdev->dev, &dev_attr_clk);
	if (ret < 0) goto err1;
	ret = device_create_file(&pdev->dev, &dev_attr_led);
	if (ret < 0) goto err1;
	register_vfd_dev(gp_vfd);
	return 0;
	err1: kfree(vfd);
	return -EINVAL;
}

static int vfd_remove(struct platform_device *pdev)
{
	struct vfd *vfd = platform_get_drvdata(pdev);
	device_remove_file(&pdev->dev, &dev_attr_clk);
	device_remove_file(&pdev->dev, &dev_attr_led);
	unregister_chrdev(vfd->config_major,vfd->config_name);
	if(vfd->config_class)
	{
		if(vfd->config_dev)
			device_destroy(vfd->config_class,MKDEV(vfd->config_major,0));
			class_destroy(vfd->config_class);
	}
	kfree(vfd);
	gp_vfd = NULL;
	return 0;	
}

static int vfd_suspend(struct platform_device* dev, pm_message_t state)
{
	int i;
	MDrv_FP_Update((char *)"@@OFF");
	for(i=0;i<1000;i++) udelay(1000);
	MDrv_FP_Update((char *)"@");
	return 0;
}

static int vfd_resume(struct platform_device* dev)
{
	int i;
	MDrv_FP_Update((char *)"@@on");
	for(i=0;i<1000;i++) udelay(1000);
	MDrv_FP_Update((char *)"@");
	return 0;
}

static struct platform_driver vfd_driver = {
	.probe   = vfd_probe,
	.remove  = vfd_remove,
	.suspend = vfd_suspend,
	.resume  = vfd_resume,
	.driver  = {
		.name           = "ember_vfd",
		.of_match_table = vfd_match
	},
};

static int __init vfd_init(void)
{
	int ret = 0;
	printk(KERN_INFO "EMBER_VFD: driver init\n");
	ember_vfd_pinmux_init();
	ret = platform_driver_register(&vfd_driver);
	return ret;
}

static void __exit vfd_exit(void)
{
	printk(KERN_INFO "EMBER_VFD: driver exit\n");
	platform_driver_unregister(&vfd_driver);
}

module_init(vfd_init);
module_exit(vfd_exit);

MODULE_AUTHOR("Corey Moyer");
MODULE_DESCRIPTION("EmbER VFD Driver");
MODULE_LICENSE("GPL");
MODULE_DEVICE_TABLE(of, vfd_match);
