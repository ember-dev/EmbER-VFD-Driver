/*
 *	Copyright (C) 2016-2019 Corey Moyer (cronmod.dev@gmail.com)
 *
 *	This file is part of Embedded Entertainment Rom Vacuum Fluorescent
 *	Display Driver (EmbER VFD Driver).
 *
 *	EmbER VFD Driver is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	EmbER VFD Driver is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with EmbER VFD Driver. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <linux/delay.h>
#include <linux/kernel.h>

#include "ember_vfd.h"

void MDrv_TM1623_Init(void);

int (*vfd_set_stb_pin_value)(int value);
int (*vfd_set_clk_pin_value)(int value);
int (*vfd_set_dat_pin_value)(int value);

typedef unsigned char U8;
unsigned char dig_value[LED_BYTE_NUM];
unsigned char bit2seg[FP_LED_NUM] = {LED_WORD5_ADDR,LED_WORD4_ADDR,LED_WORD3_ADDR,LED_WORD2_ADDR,LED_WORD1_ADDR};

static int sm1628_init(struct vfd_platform_data *pvfd_platform_data)
{
	vfd_set_stb_pin_value = pvfd_platform_data->set_stb_pin_value;
	vfd_set_clk_pin_value = pvfd_platform_data->set_clk_pin_value;
	vfd_set_dat_pin_value = pvfd_platform_data->set_dat_pin_value;
	MDrv_TM1623_Init();
	return 0;
}

int hardware_init(struct vfd_platform_data *pdev)
{
	int ret;
	ret = sm1628_init(pdev);
	return ret;
}

void MDrv_TM1623_WriteData(U8 Value)
{
	U8 i, j;
	udelay(10);

	for (i = 0; i < 8; i++){
		if (Value & 0x01) {
			PT6964_DIN_H;
		} else {
			PT6964_DIN_L;
		}
		udelay(10);
		PT6964_CLK_L;
		udelay(50);
		PT6964_CLK_H;
		udelay(10);
		Value >>= 1;
		j++;
	}
}

void MDrv_TM1623_Write_Adr_Data(U8 addr, U8 value)
{
	PT6964_STB_H;
	udelay(10);
	PT6964_STB_L;
	MDrv_TM1623_WriteData(0x44);
	PT6964_STB_H;
	udelay(10);
	PT6964_STB_L;
	MDrv_TM1623_WriteData(addr|0xc0);
	MDrv_TM1623_WriteData(value);
	PT6964_STB_H;
}

void MDrv_TM1623_Clear6964RAM(void)
{
	U8 i;
	PT6964_STB_H;
	udelay(10);
	PT6964_STB_L;
	MDrv_TM1623_WriteData(0x40); // Command 1, increment address
	PT6964_STB_H;
	udelay(10);
	PT6964_STB_L;
	MDrv_TM1623_WriteData(0xc0); // Command 2, RAM address = 0
	for(i=0;i<=13;i++) // 22 bytes
	{
		MDrv_TM1623_WriteData(0x00);
	}
	PT6964_STB_H;
}

void MDrv_TM1623_Init(void)
{
	PT6964_STB_H; // Initial state
	PT6964_CLK_H; // Intial state
	PT6964_DIN_H;
	udelay(500);
	PT6964_STB_L;
	MDrv_TM1623_WriteData(FP_LED_MODE);
	PT6964_STB_H;
	MDrv_TM1623_Clear6964RAM();
	udelay(10);
	PT6964_STB_L;
	MDrv_TM1623_WriteData(0x8F);
	PT6964_STB_H;
}

void MDrv_TM1623_Display_On(void)
{
	PT6964_STB_H;
	udelay(10);
	PT6964_STB_L;
	MDrv_TM1623_WriteData(0x8F);
	PT6964_STB_H;
}

void MDrv_TM1623_LED_DISP(char U8_Ledbit, char U8_Byte)
{
	switch (U8_Ledbit)
	{
		case 0:
			MDrv_TM1623_Write_Adr_Data(LED_WORD1_ADDR, U8_Byte);
		break;
		case 2:
			MDrv_TM1623_Write_Adr_Data(LED_WORD2_ADDR, U8_Byte);
		break;
		case 4:
			MDrv_TM1623_Write_Adr_Data(LED_WORD3_ADDR, U8_Byte);
		break;
		case 6:
			MDrv_TM1623_Write_Adr_Data(LED_WORD4_ADDR, U8_Byte);
		break;
		case 8:
			MDrv_TM1623_Write_Adr_Data(LED_WORD5_ADDR, U8_Byte);
		break;
	}
}

bool MDrv_FP_LED_CharSet(unsigned char ledBit, char U8LEDChar)
{
	unsigned char i,byte;
	printk("EMBER_VFD: display char <%c>\n", U8LEDChar);
	for (i = 0; i < sizeof(_char2SegmentTable) / sizeof (Char2Segment); i++)
	{
		if (U8LEDChar == _char2SegmentTable[i].u8Char) {
			byte = (char)_char2SegmentTable[i].u8SegmentByte;
			dig_value[bit2seg[ledBit]] = byte;
			return 0;
		}
	}
	return 1;
}

void MDrv_FP_Update(char *U8str)
{
	unsigned char i = 0,U8Data;
	// set panel data
	for(i = 0; i < LED_BYTE_NUM; i++)
	{
		dig_value[i] = 0;
	}
	for (i = 0; i < FP_LED_NUM; i++)
	{
		U8Data = U8str[i];
		if (U8Data == '\0') break;
		if (MDrv_FP_LED_CharSet(i, U8Data)) printk("EMBER_VFD: char not defined!\n");
	}
	for (i = 0; i < LED_BYTE_NUM; i++)
	{
		MDrv_TM1623_LED_DISP(i, dig_value[i]);
	}
	// enable panel
	MDrv_TM1623_Display_On();
}
