/*
 *	Copyright (C) 2016-2019 Corey Moyer (cronmod.dev@gmail.com)
 *
 *	This file is part of Embedded Entertainment Rom Vacuum Fluorescent
 *	Display Driver (EmbER VFD Driver).
 *
 *	EmbER VFD Driver is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	EmbER VFD Driver is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with EmbER VFD Driver. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef __VFD_H__
#define __VFD_H__

#define LED_BYTE_NUM			10
#define FP_LED_NUM			5 // 5 char array

#define LED_WORD1_ADDR			0 // LED 0
#define LED_WORD2_ADDR			2 // LED 2
#define LED_WORD3_ADDR			4 // LED 4
#define LED_WORD4_ADDR			6 // LED 6
#define LED_WORD5_ADDR			8 // LED 8

#define PT6964_CLK_H			vfd_set_clk_pin_value(1)
#define PT6964_CLK_L			vfd_set_clk_pin_value(0)
#define PT6964_DIN_H			vfd_set_dat_pin_value(1)
#define PT6964_DIN_L			vfd_set_dat_pin_value(0)
#define PT6964_STB_H			vfd_set_stb_pin_value(1)
#define PT6964_STB_L			vfd_set_stb_pin_value(0)

// 7-bit layout						LED 0,2,4,6
#define BIT_A				(1 << 1) //	----A----
#define BIT_B				(1 << 4) //	|	|	LED 8
#define BIT_C				(1 << 2) //	F	B	F-----------------------A
#define BIT_D				(1 << 0) //	|	|	|			|
#define BIT_E				(1 << 3) //	----G----	C-----------E-----------B
#define BIT_F				(1 << 5) //	|	|	|			|
#define BIT_G				(1 << 6) //	E	C	D-----------------------G
//							|	|
//							----D----

// character map
#define DATA_0				(BIT_A | BIT_B | BIT_C | BIT_D | BIT_E | BIT_F)
#define DATA_1				(BIT_B | BIT_C)
#define DATA_2				(BIT_A | BIT_B | BIT_D | BIT_E | BIT_G)
#define DATA_3				(BIT_A | BIT_B | BIT_C | BIT_D | BIT_G)
#define DATA_4				(BIT_B | BIT_C | BIT_F | BIT_G)
#define DATA_5				(BIT_A | BIT_C | BIT_D | BIT_F | BIT_G)
#define DATA_6				(BIT_A | BIT_C | BIT_D | BIT_E | BIT_F | BIT_G)
#define DATA_7				(BIT_A | BIT_B | BIT_C)
#define DATA_8				(BIT_A | BIT_B | BIT_C | BIT_D | BIT_E | BIT_F | BIT_G)
#define DATA_9				(BIT_A | BIT_B | BIT_C | BIT_D | BIT_F | BIT_G)
#define DATA_A				(BIT_A | BIT_B | BIT_C | BIT_E | BIT_F | BIT_G)
#define DATA_b				(BIT_C | BIT_D | BIT_E | BIT_F | BIT_G)
#define DATA_C				(BIT_A | BIT_D | BIT_E | BIT_F)
#define DATA_c				(BIT_D | BIT_E | BIT_G)
#define DATA_d				(BIT_B | BIT_C | BIT_D | BIT_E | BIT_G)
#define DATA_E				(BIT_A | BIT_D | BIT_E | BIT_F | BIT_G)
#define DATA_F				(BIT_A | BIT_E | BIT_F | BIT_G)
#define DATA_H				(BIT_B | BIT_C | BIT_E | BIT_F | BIT_G)
#define DATA_h				(BIT_C | BIT_E | BIT_F | BIT_G)
#define DATA_I				(BIT_E | BIT_F)
#define DATA_i				(BIT_E)
#define DATA_L				(BIT_D | BIT_E | BIT_F)
#define DATA_n				(BIT_C | BIT_E | BIT_G)
#define DATA_o				(BIT_C | BIT_D | BIT_E | BIT_G)
#define DATA_P				(BIT_A | BIT_B | BIT_E | BIT_F | BIT_G)
#define DATA_r				(BIT_E | BIT_G)
#define DATA_t				(BIT_D | BIT_E | BIT_F | BIT_G)
#define DATA_U				(BIT_B | BIT_C | BIT_D | BIT_E | BIT_F)
#define DATA_u				(BIT_C | BIT_D | BIT_E)
#define DATA_HYPH			(BIT_G)
#define DATA_SCORE			(BIT_D)
#define DATA_DARK			(0x00)

typedef struct
{
	char u8Char;
	char u8SegmentByte;
} Char2Segment;

static const Char2Segment _char2SegmentTable[] =
{
	{'0', DATA_0},
	{'1', DATA_1},
	{'2', DATA_2},
	{'3', DATA_3},
	{'4', DATA_4},
	{'5', DATA_5},
	{'6', DATA_6},
	{'7', DATA_7},
	{'8', DATA_8},
	{'9', DATA_9},
	{'A', DATA_A},
	{'a', DATA_A},
	{'B', DATA_b},
	{'b', DATA_b},
	{'C', DATA_C},
	{'c', DATA_c},
	{'D', DATA_d},
	{'d', DATA_d},
	{'E', DATA_E},
	{'e', DATA_E},
	{'F', DATA_F},
	{'f', DATA_F},
	{'H', DATA_H},
	{'h', DATA_h},
	{'I', DATA_I},
	{'i', DATA_i},
	{'L', DATA_L},
	{'l', DATA_L},
	{'n', DATA_n},
	{'N', DATA_n},
	{'O', DATA_0},
	{'o', DATA_o},
	{'P', DATA_P},
	{'p', DATA_P},
	{'R', DATA_r},
	{'r', DATA_r},
	{'S', DATA_5},
	{'s', DATA_5},
	{'T', DATA_t},
	{'t', DATA_t},
	{'U', DATA_U},
	{'u', DATA_u},
	{'-', DATA_HYPH},
	{'_', DATA_SCORE},
	{'@', DATA_DARK},
	{' ', DATA_DARK},
};

struct vfd_platform_data
{
	int (*set_stb_pin_value)(int value);
	int (*set_clk_pin_value)(int value);
	int (*set_dat_pin_value)(int value);
};

void MDrv_FP_Update(char *U8str);
int hardware_init(struct vfd_platform_data *pdev);

#endif // __VFD_H__
