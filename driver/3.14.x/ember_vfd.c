/*
 *	Copyright (C) 2016-2019 Corey Moyer (cronmod.dev@gmail.com)
 *
 *	This file is part of Embedded Entertainment Rom Vacuum Fluorescent
 *	Display Driver (EmbER VFD Driver).
 *
 *	EmbER VFD Driver is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	EmbER VFD Driver is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with EmbER VFD Driver. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/types.h>
#include <linux/input.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/mutex.h>
#include <linux/errno.h>
#include <asm/irq.h>
#include <asm/io.h>
#include <linux/gpio.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/err.h>
#include <linux/major.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/amlogic/aml_gpio_consumer.h>
#include <dt-bindings/gpio/gxbb.h>

#include "ember_vfd.h"

struct vfd_pins
{
	unsigned int stb_pin;
	unsigned int clk_pin;
	unsigned int dat_pin;
};

struct vfd
{
	struct vfd_pins d;
	char config_name[10];
	struct class *config_class;
	struct device *config_dev;
	int config_major;
};

static struct vfd *gp_vfd = NULL;

static int vfd_stb_pin_set_value(int value)
{
	if (value > 0)
		gpio_direction_output(gp_vfd->d.stb_pin, 1);
	else 
		gpio_direction_output(gp_vfd->d.stb_pin, 0);
	return 0;
}

static int vfd_clk_pin_set_value(int value)
{
	if (value > 0)
		gpio_direction_output(gp_vfd->d.clk_pin, 1);
	else 
		gpio_direction_output(gp_vfd->d.clk_pin, 0);
	return 0;
}

static int vfd_dat_pin_set_value(int value)
{
	if (value > 0)
		gpio_direction_output(gp_vfd->d.dat_pin, 1);
	else 
		gpio_direction_output(gp_vfd->d.dat_pin, 0);
	return 0;
}

static struct vfd_platform_data vfd_pdata =
{
	.set_stb_pin_value = vfd_stb_pin_set_value,
	.set_clk_pin_value = vfd_clk_pin_set_value,
	.set_dat_pin_value = vfd_dat_pin_set_value,
};

static const struct of_device_id vfd_match[] =
{
	{.compatible = "amlogic, ember_vfd"},
	{},
};

// sysfs - clk
static char cur_clk_value[FP_LED_NUM];

static ssize_t vfd_clk_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	int size = sizeof(cur_clk_value)-1;
	return sprintf(buf, "%.*s\n", size, cur_clk_value);
}

static ssize_t vfd_clk_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int size = sizeof(cur_clk_value);
	char val[size];
	sprintf(val, "I%s", buf);
	MDrv_FP_Update((char *)val);
	return sprintf(cur_clk_value, "%s", buf);
}

static DEVICE_ATTR(clk, S_IWUGO | S_IRUGO | S_IWUSR, vfd_clk_show, vfd_clk_store);

// sysfs - led
static char cur_led_value[FP_LED_NUM];

static ssize_t vfd_led_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	int size = sizeof(cur_led_value)-1;
	return sprintf(buf, "%.*s\n", size, cur_led_value);
}

static ssize_t vfd_led_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int size = sizeof(cur_led_value);
	char val[size];
	sprintf(val, "@%s", buf);
	MDrv_FP_Update((char *)val);
	return sprintf(cur_led_value, "%s", buf);
}

static DEVICE_ATTR(led, S_IWUGO | S_IRUGO | S_IWUSR, vfd_led_show, vfd_led_store);

static int vfd_config_open(struct inode *inode, struct file *file)
{
	file->private_data = gp_vfd;
	return 0;
}

static int vfd_config_release(struct inode *inode, struct file *file)
{
	file->private_data = NULL;
	return 0;
}

static const struct file_operations vfd_fops =
{
	.owner      = THIS_MODULE,
	.open       = vfd_config_open,
	.release    = vfd_config_release,
};

static int register_vfd_dev(struct vfd *vfd)
{
	int ret;
	strcpy(vfd->config_name,"ember_vfd");
	ret = register_chrdev(0,vfd->config_name,&vfd_fops);
	if (ret <= 0) {
		printk("EmbER_VFD: failed to register device!\r\n");
		return ret;
	}
	vfd->config_major = ret;
	printk("EmbER_VFD: config major <%d>\r\n", ret);
	vfd->config_class = class_create(THIS_MODULE, vfd->config_name);
	vfd->config_dev = device_create(vfd->config_class, NULL, MKDEV(vfd->config_major, 0), NULL, vfd->config_name);
	return ret;
}

static int vfd_dt_parse(struct platform_device *pdev)
{
	struct gpio_desc *desc;
	printk(KERN_INFO "EmbER_VFD: vfd_dt_parse start\n");
	gp_vfd = platform_get_drvdata(pdev);
	desc = of_get_named_gpiod_flags(pdev->dev.of_node, "stb_gpio", 0, NULL);
	gp_vfd->d.stb_pin = desc_to_gpio(desc);
	gpio_request(gp_vfd->d.stb_pin, "ember_vfd");
	desc = of_get_named_gpiod_flags(pdev->dev.of_node, "clk_gpio", 0, NULL);
	gp_vfd->d.clk_pin = desc_to_gpio(desc);
	gpio_request(gp_vfd->d.clk_pin, "ember_vfd");
	desc = of_get_named_gpiod_flags(pdev->dev.of_node, "dat_gpio", 0, NULL);
	gp_vfd->d.dat_pin = desc_to_gpio(desc);
	gpio_request(gp_vfd->d.dat_pin, "ember_vfd");
	printk(KERN_INFO "EmbER_VFD: vfd_dt_parse done\n");
	return 0;
}

static void vfd_pinmux_init(struct platform_device *pdev)
{
	vfd_dt_parse(pdev);
	gpio_direction_output(gp_vfd->d.dat_pin,0);
	gpio_direction_output(gp_vfd->d.stb_pin,1);
	gpio_direction_output(gp_vfd->d.dat_pin,1);
}

static void vfd_pinmux_deinit(struct platform_device *pdev)
{
	gpio_free(gp_vfd->d.stb_pin);
	gpio_free(gp_vfd->d.clk_pin);
	gpio_free(gp_vfd->d.dat_pin);
}

static int vfd_probe(struct platform_device *pdev)
{
	struct vfd *vfd;
	int ret;
	struct vfd_platform_data *pdata;
	vfd = kzalloc(sizeof(struct vfd), GFP_KERNEL);
	if (!vfd)
		goto err1;
	gp_vfd = vfd;
	platform_set_drvdata(pdev, vfd);
	vfd_pinmux_init(pdev);
	pdata = &vfd_pdata;
	if (!pdata) {
		dev_err(&pdev->dev, "EmbER_VFD: platform data not found!\n");
		return -EINVAL;
	}
	if (!pdev->dev.of_node) {
		dev_err(&pdev->dev, "EmbER_VFD: pdev->dev.of_node == NULL!\n");
		return -1;
	}
	if (hardware_init(pdata)) {
		dev_err(&pdev->dev, "EmbER_VFD: hardware init failed!\n");
		return -EINVAL;
	}
	register_vfd_dev(gp_vfd);
	ret = device_create_file(&pdev->dev, &dev_attr_clk);
	if (ret < 0) goto err1;
	ret = device_create_file(&pdev->dev, &dev_attr_led);
	if (ret < 0) goto err1;
	return 0;
	err1: kfree(vfd);
	return -EINVAL;
}

static int vfd_remove(struct platform_device *pdev)
{
	struct vfd *vfd = platform_get_drvdata(pdev);
	device_remove_file(&pdev->dev, &dev_attr_clk);
	device_remove_file(&pdev->dev, &dev_attr_led);
	unregister_chrdev(vfd->config_major, vfd->config_name);
	if (vfd->config_class) {
		if (vfd->config_dev)
			device_destroy(vfd->config_class, MKDEV(vfd->config_major, 0));
		class_destroy(vfd->config_class);
	}
	vfd_pinmux_deinit(pdev);
	kfree(vfd);
	gp_vfd = NULL;
	return 0;	
}

static int vfd_suspend(struct platform_device* dev, pm_message_t state)
{
	int i;
	MDrv_FP_Update((char *)"@@OFF");
	for (i = 0; i < 1000; i++) udelay(1000);
	MDrv_FP_Update((char *)"@");
	return 0;
}

static int vfd_resume(struct platform_device* dev)
{
	int i;
	MDrv_FP_Update((char *)"@@on");
	for (i = 0; i < 1000; i++) udelay(1000);
	MDrv_FP_Update((char *)"@");
	return 0;
}

static struct platform_driver vfd_driver =
{
	.probe   = vfd_probe,
	.remove  = vfd_remove,
	.suspend = vfd_suspend,
	.resume  = vfd_resume,
	.driver  = {
		.name		= "ember_vfd",
		.owner		= THIS_MODULE,
		.of_match_table	= vfd_match
	},
};

static int __init vfd_init(void)
{
	int ret;
	printk(KERN_INFO "EmbER_VFD: driver init\n");
	ret = platform_driver_register(&vfd_driver);
	return ret;
}

static void __exit vfd_exit(void)
{
	printk(KERN_INFO "EmbER_VFD: driver exit\n");
	platform_driver_unregister(&vfd_driver);
}

module_init(vfd_init);
module_exit(vfd_exit);

MODULE_AUTHOR("Corey Moyer");
MODULE_DESCRIPTION("EmbER VFD Driver");
MODULE_LICENSE("GPL");
MODULE_DEVICE_TABLE(of, vfd_match);
